package org.xutils.image;
import ohos.agp.animation.Animator;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.media.image.PixelMap;
import org.xutils.common.util.LogUtil;

import java.lang.reflect.Method;

/**
 * Created by wyouflf on 15/10/13.
 * ImageView Animation Helper
 */
public final class ImageAnimationHelper {

    private final static Method cloneMethod;

    static {
        Method method = null;
        try {
            method = Animator.class.getDeclaredMethod("clone");
            method.setAccessible(true);
        } catch (Throwable ex) {
            method = null;
            LogUtil.w(ex.getMessage(), ex);
        }
        cloneMethod = method;
    }

    private ImageAnimationHelper() {
    }

    public static void fadeInDisplay(final Image imageView, PixelMap drawable) {

    }

    public static void animationDisplay(Image imageView, PixelMap drawable, Animator animation) {

    }
}
