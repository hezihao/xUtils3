package org.xutils.http.app;


import java.lang.reflect.Type;

/**
 * Created by wyouflf on 15/8/4.
 * {@link org.xutils.http.annotation.HttpResponse} 注解的返回值转换模板
 *
 * @param <ResponseDataType> 支持String, byte[], JSONObject, JSONArray, InputStream
 */
public interface ResponseParser<ResponseDataType> extends RequestInterceptListener {


    Object parse(Type resultType, Class<?> resultClass, ResponseDataType result) throws Throwable;
}
