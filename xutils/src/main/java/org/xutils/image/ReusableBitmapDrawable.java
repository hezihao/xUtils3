package org.xutils.image;

import ohos.agp.components.element.PixelMapElement;
import ohos.media.image.PixelMap;

/*package*/ final class ReusableBitmapDrawable extends PixelMap implements ReusableDrawable {

    private MemCacheKey key;

    protected ReusableBitmapDrawable(long nativeImagePixelMap, long nativeAllocBytes) {
        super(nativeImagePixelMap, nativeAllocBytes);
    }


    @Override
    public MemCacheKey getMemCacheKey() {
        return key;
    }

    @Override
    public void setMemCacheKey(MemCacheKey key) {
        this.key = key;
    }
}
