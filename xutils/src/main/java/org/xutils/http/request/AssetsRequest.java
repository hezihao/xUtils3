package org.xutils.http.request;


import ohos.app.Context;
import ohos.global.resource.ResourceManager;
import org.xutils.http.RequestParams;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;

/**
 * Created by wyouflf on 15/11/4.
 * Assets资源文件请求
 */
public class AssetsRequest extends ResRequest {

    public AssetsRequest(RequestParams params, Type loadType) throws Throwable {
        super(params, loadType);
    }

    @Override
    public InputStream getInputStream() throws IOException {
        if (inputStream == null) {
            Context context = params.getContext();
            String assetsPath = queryUrl.replace("assets://", "");
            //TODO 读取资源文件
//            inputStream = context.getResources().getAssets().open(assetsPath);
            ResourceManager resourceManager=context.getResourceManager();
            contentLength=resourceManager.getRawFileEntry(assetsPath).openRawFile().available();
        }
        return inputStream;
    }
}
