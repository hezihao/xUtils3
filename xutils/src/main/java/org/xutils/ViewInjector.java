package org.xutils;


import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;

/**
 * Created by wyouflf on 15/10/29.
 * view注入接口
 */
public interface ViewInjector {


    void inject(Component view);


    void inject(AbilitySlice activity);


    void inject(Object handler, Component view);

}
