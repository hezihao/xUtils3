package org.xutils.http.app;

import org.xutils.http.RequestParams;
import org.xutils.http.request.UriRequest;

/**
 * Created by wyouflf on 15/11/12.
 * 请求重定向控制接口
 */
public interface RedirectHandler {


    RequestParams getRedirectParams(UriRequest request) throws Throwable;
}
