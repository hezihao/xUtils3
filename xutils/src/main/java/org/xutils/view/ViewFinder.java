package org.xutils.view;


import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;

/**
 * Author: wyouflf
 * Date: 13-9-9
 * Time: 下午12:29
 */
/*package*/ final class ViewFinder {

    private Component view;
    private AbilitySlice activity;

    public ViewFinder(Component view) {
        this.view = view;
    }

    public ViewFinder(AbilitySlice activity) {
        this.activity = activity;
    }

    public Component findViewById(int id) {
        if (view != null) return view.findComponentById(id);
        if (activity != null) return activity.findComponentById(id);
        return null;
    }

    public Component findViewByInfo(ViewInfo info) {
        return findViewById(info.value, info.parentId);
    }

    public Component findViewById(int id, int pid) {
        Component pView = null;
        if (pid > 0) {
            pView = this.findViewById(pid);
        }

        Component view = null;
        if (pView != null) {
            view = pView.findComponentById(id);
        } else {
            view = this.findViewById(id);
        }
        return view;
    }
}
