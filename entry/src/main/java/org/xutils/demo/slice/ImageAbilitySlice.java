/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.xutils.demo.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.media.image.PixelMap;
import org.xutils.common.Callback;
import org.xutils.common.util.LogUtil;
import org.xutils.demo.ResourceTable;
import org.xutils.image.ImageOptions;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * 图片加载示例
 */
public class ImageAbilitySlice extends AbilitySlice {


    @ViewInject(ResourceTable.Id_img_photo)
    Image imgPhoto;

    ImageOptions imageOptions;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_image);
        x.view().inject(this);

        imageOptions = new ImageOptions.Builder()
                .setSize(500,500)
                .setRadius(5)
                // 如果ImageView的大小不是定义为wrap_content, 不要crop.
                .setCrop(true) // 很多时候设置了合适的scaleType也不需要它.
                // 加载中或错误图片的ScaleType
                //.setPlaceholderScaleType(ImageView.ScaleType.MATRIX)
                .setImageScaleType(Image.ScaleMode.CENTER)
                .setLoadingDrawableId(ResourceTable.Media_ic_test_2)
                .setFailureDrawableId(ResourceTable.Media_icon)
                .setIgnoreGif(false)
                .build();
        x.image().bind(imgPhoto, "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fattach.bbs.miui.com%2Fforum%2F201303%2F16%2F173710lvx470i4348z6i6z.jpg&refer=http%3A%2F%2Fattach.bbs.miui.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616206430&t=9ed756d70c69e6c3d04923c511ba93c5", new Callback.CommonCallback<PixelMap>() {
            @Override
            public void onSuccess(PixelMap result) {
                LogUtil.e("图片请求成功=="+result);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                LogUtil.e("图片请求error=="+ex.toString());
            }

            @Override
            public void onCancelled(CancelledException cex) {
                LogUtil.e("图片请求onCancelled=="+cex.toString());

            }

            @Override
            public void onFinished() {
                LogUtil.e("图片请求onFinished");
            }
        });
    }

}
