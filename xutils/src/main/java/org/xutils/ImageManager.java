package org.xutils;


import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.media.image.PixelMap;
import org.xutils.common.Callback;
import org.xutils.image.ImageOptions;

import java.io.File;

/**
 * Created by wyouflf on 15/6/17.
 * 图片绑定接口
 */
public interface ImageManager {

    void bind(Image view, String url);

    void bind(Image view, String url, ImageOptions options);

    void bind(Image view, String url, Callback.CommonCallback<PixelMap> callback);

    void bind(Image view, String url, ImageOptions options, Callback.CommonCallback<PixelMap> callback);

    Callback.Cancelable loadDrawable(String url, ImageOptions options, Callback.CommonCallback<PixelMap> callback);

    Callback.Cancelable loadFile(String url, ImageOptions options, Callback.CacheCallback<File> callback);

    void clearMemCache();

    void clearCacheFiles();
}
