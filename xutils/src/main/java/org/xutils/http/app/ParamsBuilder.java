package org.xutils.http.app;

import org.xutils.http.RequestParams;
import org.xutils.http.annotation.HttpRequest;

import javax.net.ssl.SSLSocketFactory;

/**
 * Created by wyouflf on 15/8/20.
 * <p>
 * {@link org.xutils.http.annotation.HttpRequest} 注解的参数构建的模板接口
 */
public interface ParamsBuilder {


    String buildUri(RequestParams params, HttpRequest httpRequest) throws Throwable;


    String buildCacheKey(RequestParams params, String[] cacheKeys);

    SSLSocketFactory getSSLSocketFactory() throws Throwable;


    void buildParams(RequestParams params) throws Throwable;


    void buildSign(RequestParams params, String[] signs) throws Throwable;
}
