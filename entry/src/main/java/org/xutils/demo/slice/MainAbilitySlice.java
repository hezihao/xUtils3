/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.xutils.demo.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import org.xutils.common.Callback;
import org.xutils.common.util.LogUtil;
import org.xutils.demo.ResourceTable;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * 示例
 */
public class MainAbilitySlice extends AbilitySlice {

    @ViewInject(ResourceTable.Id_btn_viewbind)
    Button buttonTest;

    @ViewInject(ResourceTable.Id_btn_http_get)
    Button btnHttpGet;


    @ViewInject(ResourceTable.Id_text_show)
    Text txtShow;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        x.view().inject(this);
    }

    @Event(value = {ResourceTable.Id_btn_viewbind, ResourceTable.Id_btn_http_get, ResourceTable.Id_img_photo, ResourceTable.Id_btn_db, ResourceTable.Id_btn_uploadown})
    private void onTest1Click(Component view) {
        if (view.getId() == ResourceTable.Id_btn_viewbind) {
            buttonTest.setText("绑定事件成功");
        }
        if (view.getId() == ResourceTable.Id_btn_http_get) {
            sendHttpGet();
        }
        if (view.getId() == ResourceTable.Id_img_photo) {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withAction("action.image.slice")
                    .build();
            intent.setOperation(operation);
            startAbilityForResult(intent, 1);
        }
        if (view.getId() == ResourceTable.Id_btn_db) {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withAction("action.db.slice")
                    .build();
            intent.setOperation(operation);
            startAbilityForResult(intent, 1);
        }
        if (view.getId() == ResourceTable.Id_btn_uploadown) {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withAction("action.upload.slice")
                    .build();
            intent.setOperation(operation);
            startAbilityForResult(intent, 1);
        }
    }

    private void sendHttpGet() {
        btnHttpGet.setText("网络请求");
        RequestParams params = new RequestParams("https://api.github.com/users/wqzy");
        x.http().get(params, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                LogUtil.e("请求成功" + result);
                btnHttpGet.getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        txtShow.setText("请求结果:" + result + "");
                    }
                });

            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                LogUtil.e("请求onError" + ex.toString());
                btnHttpGet.getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        btnHttpGet.setText("请求报错");
                        txtShow.setText("请求结果:" + ex.toString() + "");

                    }
                });
            }

            @Override
            public void onCancelled(CancelledException cex) {
            }

            @Override
            public void onFinished() {
                LogUtil.e("请求onFinished");
            }
        });

    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
