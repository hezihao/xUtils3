package org.xutils.common;

import org.xutils.common.task.AbsTask;

/**
 * Created by wyouflf on 15/6/11.
 * 任务管理接口
 */
public interface TaskController {


    void autoPost(Runnable runnable);


    void post(Runnable runnable);


    void postDelayed(Runnable runnable, long delayMillis);


    void run(Runnable runnable);

    void removeCallbacks(Runnable runnable);


    <T> AbsTask<T> start(AbsTask<T> task);


    <T> T startSync(AbsTask<T> task) throws Throwable;


    <T extends AbsTask<?>> Callback.Cancelable startTasks(Callback.GroupCallback<T> groupCallback, T... tasks);
}
