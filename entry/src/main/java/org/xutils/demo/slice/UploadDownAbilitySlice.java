/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.xutils.demo.slice;

import org.xutils.demo.ResourceTable;
import org.xutils.demo.download.DownloadInfo;
import org.xutils.demo.download.DownloadManager;
import org.xutils.demo.download.DownloadViewHolder;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import org.xutils.common.Callback;
import org.xutils.common.util.LogUtil;
import org.xutils.ex.DbException;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.Event;
import org.xutils.x;

import java.io.File;

/**
 * 上传、下载
 */
public class UploadDownAbilitySlice extends AbilitySlice {


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_upload_down);
        x.view().inject(this);
    }

    @Event(value = {ResourceTable.Id_btn_upload, ResourceTable.Id_btn_down})
    private void onTest1Click(Component view) throws DbException {
        //下载
        if (view.getId() == ResourceTable.Id_btn_down) {
//            File file=new File(x.app().getFilesDir()+"/aaa.png");
            String url = "https://github.com/zcweng/SwitchButton/raw/master/device-capture.png";
            DownloadManager.getInstance().startDownload(
                    url, "bbb",
                    x.app().getFilesDir() + "/aaa.png", true, false, new DownloadViewHolder(view, new DownloadInfo()) {
                        @Override
                        public void onWaiting() {

                        }

                        @Override
                        public void onStarted() {

                        }

                        @Override
                        public void onLoading(long total, long current) {
                        }

                        @Override
                        public void onSuccess(File result) {
                            LogUtil.e("下载成功");
                        }

                        @Override
                        public void onError(Throwable ex, boolean isOnCallback) {
                            LogUtil.e("下载onError" + ex.toString());

                        }

                        @Override
                        public void onCancelled(Callback.CancelledException cex) {

                        }
                    });
        }

        //上传
        if (view.getId() == ResourceTable.Id_btn_upload) {
            RequestParams params = new RequestParams("https://httpbin.org/post");
            params.addQueryStringParameter("wd", "xUtils");
            params.setMultipart(true);
            params.addBodyParameter(
                    "file",
                    new File(x.app().getFilesDir() + "/aaa.png"),
                    null); // 如果文件没有扩展名, 最好设置contentType参数.
            x.http().post(params, new Callback.CommonCallback<String>() {
                @Override
                public void onSuccess(String result) {
                    LogUtil.e("上传成功");
                }

                @Override
                public void onError(Throwable ex, boolean isOnCallback) {
                    LogUtil.e("上传onError" + ex.toString());
                }

                @Override
                public void onCancelled(CancelledException cex) {
                }

                @Override
                public void onFinished() {

                }
            });
        }
    }

}
